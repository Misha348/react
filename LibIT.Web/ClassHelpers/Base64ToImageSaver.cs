﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using LibIT.Web.ExtentionMethods;

namespace LibIT.Web.ClassHelpers
{
	public class Base64ToImageSaveHelper
	{
		public string Base64 { get; set; }
		public string NewFileName { get; set; }
		public Bitmap Img { get; set; }
		public string DirectotyToSaveFile { get; set; }

		public Base64ToImageSaveHelper(){}

		public Bitmap GetImagefromBase64(string imageAsBase64)
		{
			if (imageAsBase64.Contains(","))
				Base64 = imageAsBase64.Split(',')[1];
			Img = Base64.FromBase64StringToImage();
			return Img;
		}

		public string GetDirectoryToSaveFile(string directotyName)
		{
			if (!Directory.Exists(Path.Combine(Directory.GetCurrentDirectory(),
				directotyName)))
			{
				Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(),
					directotyName));
			}
			DirectotyToSaveFile = 
				Path.Combine(Directory.GetCurrentDirectory(), directotyName);
			return DirectotyToSaveFile;
		}

		public string GetRandomFileName()
		{
			string ext = ".jpg";
			return Path.GetRandomFileName() + ext;
		}
	}
}
