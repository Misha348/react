﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LibIT.Web.ClassHelpers
{
	public class ImageManager
	{
		private static string serverPath;
		private static string folderName;
		private static string uploadFolderPath;

		public string Ext { get; set; }
		public string FileOldName { get; set; }
		public string FileNewName { get; set; }
		public string SavedFilePath { get; set; }
		public IFormFile NewImageFile { get; set; }

		static ImageManager()
		{
			serverPath = Directory.GetCurrentDirectory();
			folderName = "Uploads";
			uploadFolderPath = Path.Combine(serverPath, folderName);
		}

		public ImageManager(IFormFile newFile)
		{
			NewImageFile = newFile;
			FileOldName = newFile.FileName;
		}

		public ImageManager()
		{ }

		public static string GetServerPath()
		{
			return serverPath;
		}

		public static string GetFolderName()
		{
			return folderName;
		}

		public static string GetUploadFolderPath()
		{
			return uploadFolderPath;
		}

		public string GetFileExtention()
		{
			Ext = Path.GetExtension(FileOldName);
			return Ext;
		}

		public string GetFileName()
		{
			FileNewName = Path.GetRandomFileName() + GetFileExtention();
			return FileNewName;
		}

		public string GetSavedFilePath()
		{
			SavedFilePath = Path.Combine(uploadFolderPath, GetFileName());
			return SavedFilePath;
		}

		public void CheckUploadFolderExistence(string _folderName)
		{
			if (!Directory.Exists(Path.Combine(serverPath, _folderName)))
			{
				Directory.CreateDirectory(Path.Combine(serverPath, _folderName));
			}
		}

		public void SaveFileToServer()
		{
			using (var fileStream = new FileStream(GetSavedFilePath(), FileMode.Create))
			{
				NewImageFile.CopyTo(fileStream);
			}
		}
	}
}
