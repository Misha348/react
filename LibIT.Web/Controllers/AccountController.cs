﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using LibIT.Web.ClassHelpers;
using LibIT.Web.Entities;
using LibIT.Web.ExtentionMethods;
using LibIT.Web.Models.User;
using LibIT.Web.Services.JWT;

namespace LibIT.Web.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AccountController : ControllerBase
	{
		private readonly EFContext _context;
        private readonly RoleManager<DbRole> _roleManager;
        private readonly UserManager<DbUser> _userManager;
		private readonly SignInManager<DbUser> _signInManager;
		private readonly IJwtTokenService _IJwtTokenService;

		public AccountController(EFContext context,
		   UserManager<DbUser> userManager,
		   SignInManager<DbUser> signInManager,
           RoleManager<DbRole> roleManager, 
           IJwtTokenService IJwtTokenService)
		{
			_context = context;
			_userManager = userManager;
			_signInManager = signInManager;
            _roleManager = roleManager;
            _IJwtTokenService = IJwtTokenService;
		}

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody] UserLoginVM model)
        {
            if (!ModelState.IsValid)
            {
                //var errrors = CustomValidator.GetErrorsByModel(ModelState);
                return BadRequest("Bad Model");
            }

            var user = _context.Users.FirstOrDefault(u => u.Email == model.Email);
            if (user == null)
            {
                return BadRequest(new { invalid = "Such user not found" });
            }

            var result = _signInManager
                .PasswordSignInAsync(user, model.Password, false, false).Result;

            if (!result.Succeeded)
            {
                return BadRequest(new { invalid = "incorrect filled in password" });
            }

            await _signInManager.SignInAsync(user, isPersistent: false);

            return Ok(
                new
                {
                    token = _IJwtTokenService.CreateToken(user)
                });
        }

		[HttpPost("register")]
		public async Task<IActionResult> Register([FromBody] UserRegisterVM model)
		{
            bool isEmailExist = await _userManager.FindByEmailAsync(model.Email) == null;
           
            if (!isEmailExist)
            {
                return BadRequest("Such email address already exist.");
            }

            if (ModelState.IsValid)
            {
                #region manual base64 saving
                //string base64 = model.ImageBase64;
                //if (base64.Contains(","))
                //{
                //    base64 = base64.Split(',')[1];
                //}
                //var img = base64.FromBase64StringToImage();              
                //                                        // serverPath = Directory.GetCurrentDirectory();
                //var directoryPathToSaveImg = Path.Combine(ImageManager.GetServerPath(), "Uploads");
                //string ext = ".jpg";
                //string fileName = Path.GetRandomFileName() + ext;
                //string fullPathToSaveImg = Path.Combine(directoryPathToSaveImg, fileName);
                //img.Save(fullPathToSaveImg, ImageFormat.Jpeg);
                #endregion
                Base64ToImageSaveHelper helper = new Base64ToImageSaveHelper();
                var img = helper.GetImagefromBase64(model.ImageBase64);
                var directoryPathToSaveImg = helper.GetDirectoryToSaveFile("Uploads");
                string fileName = helper.GetRandomFileName();
                string fullPathToSaveImg = Path.Combine(directoryPathToSaveImg, fileName);
                img.Save(fullPathToSaveImg, ImageFormat.Jpeg);

                var user = new DbUser
                {
                    FirstName = model.FirstName, 
                    LastName = model.LastName, 
                    Email = model.Email, 
                    PhoneNumber = model.PhoneNumber,
                    UserName = model.Email, 
                    Image = fileName,                     
                };

                var result = await _userManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, "User");
                    return Ok();

                    //return Ok(new
                    //      {
                    //          token = _IJwtTokenService.CreateToken(user)
                    //      });
                    //return Created("", result);
                }
                    
            }
            return BadRequest("Invalid user data. Change your data.");
        }
	}
}
