﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using LibIT.Web.Entities;
using LibIT.Web.Models.User;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using LibIT.Web.Services.JWT;
using System.IO;
using System.Drawing;
using LibIT.Web.ExtentionMethods;
using LibIT.Web.ClassHelpers;
using System.Drawing.Imaging;

namespace LibIT.Web.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Authorize]
	public class ProfileController : ControllerBase
	{	
		private readonly UserManager<DbUser> _userManager;
		private readonly EFContext _context;

		public ProfileController(EFContext context,
		   UserManager<DbUser> userManager)
		{			
			_userManager = userManager;
			_context = context;
		}

		[HttpGet("profileinfo")]
		public async Task<IActionResult> ProfileInfo(/*int id*/)
		{	
			var userName = User.Claims.FirstOrDefault(x => x.Type == "name").Value;
			var user = await _userManager.FindByNameAsync(userName);
			
			if (user == null)
				return BadRequest("Error, such user doesnt exist");

			var userImgServerPath = Path.Combine(Directory.GetCurrentDirectory(), "Uploads", user.Image);
			string userImgAsBase64;
			using ( Image image = Image.FromFile(userImgServerPath) )
			{
				using (MemoryStream m = new MemoryStream())
				{
					image.Save(m, image.RawFormat);
					byte[] imageBytes = m.ToArray();					
					userImgAsBase64 = "data:image/jpeg;base64," + Convert.ToBase64String(imageBytes);					
				}
			}

			UserProfileInfoVM userProfileInfo = new UserProfileInfoVM
			{ 
				//Id = (int)user.Id, 
				FirstName = user.FirstName,
				LastName = user.LastName,
				Email = user.Email,
				PhoneNumber = user.PhoneNumber,  
				ProfileImage = userImgAsBase64
			};
			return Ok(userProfileInfo);
			
		}

		[HttpPut("updateprofile")]
		public async Task<IActionResult> UpdateUserProfile( UpdatedUserProfileVM model)
		{
			var userName = User.Claims.FirstOrDefault(x => x.Type == "name").Value;
			var user = await _userManager.FindByNameAsync(userName);

			if (user == null)
				return BadRequest("Error, such user doesnt exist");

			string oldPhotoPath = Path.Combine(Directory.GetCurrentDirectory(), "Uploads", user.Image);
			System.IO.File.Delete(oldPhotoPath);

			Base64ToImageSaveHelper helper = new Base64ToImageSaveHelper();
			var img = helper.GetImagefromBase64(model.ProfileImage);			
			var directoryPathToSaveImg = helper.GetDirectoryToSaveFile("Uploads");
			string fileName = helper.GetRandomFileName();
			string fullPathToSaveImg = Path.Combine(directoryPathToSaveImg, fileName);
			img.Save(fullPathToSaveImg, ImageFormat.Jpeg);

			user.FirstName = model.FirstName;
			user.LastName = model.LastName;
			user.Image = fileName;
			user.PhoneNumber = model.PhoneNumber;
			//user.Email = model.Email;

			await Task.Run(() => _context.Update(user));
			_context.SaveChanges();
			return Ok();
		}
	}
}
