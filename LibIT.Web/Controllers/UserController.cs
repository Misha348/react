﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using LibIT.Web.ClassHelpers;
using LibIT.Web.Entities;
using LibIT.Web.ExtentionMethods;
using LibIT.Web.Models.User;

namespace LibIT.Web.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UsersController : ControllerBase
	{
		private readonly EFContext _context;
		private readonly UserManager<DbUser> _userManager;
        public UsersController(UserManager<DbUser> userManager, EFContext context)
        {           
            _userManager = userManager;
			_context = context;
		}

        [HttpGet("")]
        public async Task<IActionResult> GetAllUsers()
        {
            var users = await _userManager.Users.Select(x => new UsersInfoVM
            {
                Id = x.Id,
                UserImage = x.Image,    
                FirstName = x.FirstName, 
                LastName = x.LastName,
                Email = x.Email, 
                Phone = x.PhoneNumber,               
            }).ToListAsync();

			foreach (var user in users)
			{
				var userImgServerPath = Path.Combine(Directory.GetCurrentDirectory(), "Uploads", user.UserImage);
				if (System.IO.File.Exists(userImgServerPath))
				{
					string userImgAsBase64;
					using (Image image = Image.FromFile(userImgServerPath))
					{
						using (MemoryStream m = new MemoryStream())
						{
							image.Save(m, image.RawFormat);
							byte[] imageBytes = m.ToArray();
							userImgAsBase64 = "data:image/jpeg;base64," + Convert.ToBase64String(imageBytes);
						}
					}
					user.UserImage = userImgAsBase64;
				}				
			}
            return Ok(users);
        }

		[HttpGet("details/{id}")]
		public async Task<IActionResult> Details(int id)
		{
			var user = await _userManager.Users.Select(x => new UserInfoDetails
			{
				Id = x.Id, 
				UserImage = x.Image,
				FirstName = x.FirstName,
				LastName = x.LastName,
				Email = x.Email,
				Phone = x.PhoneNumber, 
			}).SingleOrDefaultAsync(x => x.Id == id);
			return Ok(user);
		}

		[HttpPost("create")]
		public async Task<IActionResult> AddUser([FromBody] AddUserVM model)
		{
			bool isEmailExist = await _userManager.FindByEmailAsync(model.Email) == null;

			if (!isEmailExist)			
				return BadRequest("Such email address already exist. Try another");			

			if (ModelState.IsValid)
			{
				#region manual base64 saving
				//string base64 = model.UserImage;
				//if (base64.Contains(","))
				//{
				//	base64 = base64.Split(',')[1];
				//}
				//var img = base64.FromBase64StringToImage();				
				//var directoryPathToSaveImg = Path.Combine(ImageManager.GetServerPath(), "Uploads");
				//string ext = ".jpg";
				//string fileName = Path.GetRandomFileName() + ext;
				//string fullPathToSaveImg = Path.Combine(directoryPathToSaveImg, fileName);
				//img.Save(fullPathToSaveImg, ImageFormat.Jpeg);
				#endregion
				Base64ToImageSaveHelper helper = new Base64ToImageSaveHelper();
				var img = helper.GetImagefromBase64(model.UserImage);
				var directoryPathToSaveImg = helper.GetDirectoryToSaveFile("Uploads");
				string fileName = helper.GetRandomFileName();
				string fullPathToSaveImg = Path.Combine(directoryPathToSaveImg, fileName);
				img.Save(fullPathToSaveImg, ImageFormat.Jpeg);

				var user = new DbUser
				{
					FirstName = model.FirstName,
					LastName = model.LastName,
					Email = model.Email,
					PhoneNumber = model.Phone,
					UserName = model.Email,
					Image = fileName
				};
				var result = await _userManager.CreateAsync(user, model.Password);
				if (result.Succeeded)
				{
					await _userManager.AddToRoleAsync(user, "User");
					return Ok();	
				}
			}
			return BadRequest("Impossible to create new user");
		}	

		
		[HttpGet("edit/{id}")]
		public async Task<IActionResult> EditUser(int id)
		{
			var user = await _userManager.Users.Select(x => new EditUserInfoVM
			{
				Id = x.Id, 
				UserImage = x.Image,
				FirstName = x.FirstName,
				LastName = x.LastName,
				Email = x.Email,
				Phone = x.PhoneNumber, 
			}).SingleOrDefaultAsync(x => x.Id == id);
			return Ok(user);
		}
		
		[HttpPut("update")]
		public async Task<IActionResult> UpdateUser(EditUserVM model)
		{
			var user = await _userManager.Users.SingleOrDefaultAsync(x => x.Id == model.Id);

			user.FirstName = model.FirstName;
			user.LastName = model.LastName;
			// delete old photo from server + add new photo to server; or
			// check wether new photo is same as old

			string oldPhotoPath = Path.Combine(Directory.GetCurrentDirectory(), "Uploads", user.Image);
			System.IO.File.Delete(oldPhotoPath);

			Base64ToImageSaveHelper helper = new Base64ToImageSaveHelper();
			var img = helper.GetImagefromBase64(model.UserImage);
			var directoryPathToSaveImg = helper.GetDirectoryToSaveFile("Uploads");
			string fileName = helper.GetRandomFileName();
			string fullPathToSaveImg = Path.Combine(directoryPathToSaveImg, fileName);
			img.Save(fullPathToSaveImg, ImageFormat.Jpeg);

			user.Image = fileName;
			user.PhoneNumber = model.Phone;
			// user.Email = model.Email;

			await Task.Run( () => _userManager.UpdateAsync(user));
			_context.SaveChanges();

			return Ok();
		}

		[HttpDelete("delete/{id}")]
		public async Task<IActionResult> DeleteUser(int id)
		{
			var user = await _userManager.FindByIdAsync(id.ToString());			

			if (user == null)
				return BadRequest("impossible to find appropriate user to delete");

			var result = await _userManager.DeleteAsync(user);
			if (result.Succeeded)
			{
				string oldPhotoPath = Path.Combine(Directory.GetCurrentDirectory(), "Uploads", user.Image);
				System.IO.File.Delete(oldPhotoPath);
				return Ok(/*?*/);
			}

			return BadRequest("impossible to delete user");
		}
	}
}
