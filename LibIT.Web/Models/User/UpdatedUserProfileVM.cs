﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibIT.Web.Models.User
{
	public class UpdatedUserProfileVM
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string ProfileImage { get; set; }
		public string PhoneNumber { get; set; }		
		public string Email { get; set; }
	}
}
