﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibIT.Web.Models.User
{
	public class UserRegisterVM
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string ImageBase64 { get; set; }
		public string Email { get; set; }
		public string PhoneNumber { get; set; }
		public string Password { get; set; }
		public string PasswordConfirmation { get; set; }
	}
}

