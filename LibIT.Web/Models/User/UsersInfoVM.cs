﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LibIT.Web.Models.User
{
	public class UsersInfoVM
	{
		public long Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string UserImage { get; set; }
		//public bool EmailConfirmed { get; set; }
		public string Phone { get; set; }
		public string Email { get; set; }
	}
}
