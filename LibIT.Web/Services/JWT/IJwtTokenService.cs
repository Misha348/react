﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibIT.Web.Entities;

namespace LibIT.Web.Services.JWT
{
	public interface IJwtTokenService
	{
		string CreateToken(DbUser user);
	}
}
