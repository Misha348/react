import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import { Link } from 'react-router-dom';
import { serverUrl} from '../../Config';
import axios from 'axios';
import EclipseWidget from '../../common/loader-spinner';


class LoginPage extends Component {
  state = {
    redirect: false, 
    loading: false,  
    email: "",   
    password: "",    
    errorMessage: "",
    errors: { }     
  } 

  onSubmitForm = (e) => {
    e.preventDefault();

    const {email, password, redirect} = this.state;     
    let errors = {};
    let  errorMessage = "";
    console.log("PRE MODEL:", email, password)

    const emailRegex = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/;
    const passwdRegex = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;

    if (email.trim() === "" || !emailRegex.test(email.trim()))   
        errorMessage = "Email or password invalid";   
    //       errors = {...errors, email: "email is not valid"}; 
    // else
    //       errors = {...errors, email: ""};

    if (password.trim() === "" || !passwdRegex.test(password.trim()))  
        errorMessage = "Email or password invalid";     
          // errors = {...errors, password: "Password is not valid; at least 1 Upper case letter; " + 
          //                                 "at least 1 Lowwer case letter; at least 1 digit; " + 
          //                                 " at least 1 theese sights: !@#)($%^&*"};
    // else
    //       errors = {...errors, password: ""};                                      

    const isValid = errorMessage === "";  
    if (isValid) {   
      const url = `${serverUrl}api/Account/login`;

      const model = {        
        Email: email,        
        Password: password
      };  

      console.log("MODEL:", model)
      
      axios.post(url, model)
      .then(
       (resp) => { 
        console.log("GOOD:", resp)       
          this.setState({loading: false });
          this.setState({ redirect: true })
       },
       (badResp) => {   
        console.log("BAD:", badResp)    
        this.setState({ errorMessage: "Such user not found", errors, loading: true });    
          this.setState({loading: false });
       }
      )
     .catch(err => {
         this.setState({loading: false });
         console.log("Global server prooblem in controller message", err);
     });

      this.setState({ errorMessage: "", errors, loading: true });   
    }
    else {
      this.setState({ errorMessage: "Email or login invalid"});
    }
  }

  handlerChange = (e) => {           
    this.setState({[e.target.name]: e.target.value});     
  }

  render() { 
    const { email, password, errorMessage, 
      errors, loading, redirect} = this.state;

    if (redirect) {
      return <Redirect to='/Profile'/>;
    }

    return ( 
      <>
        <div className="container">
           <br></br>
           <p className="text-center">More bootstrap 4 components on <Link to="http://bootstrap-ecommerce.com/"> Bootstrap-ecommerce.com</Link></p>
           <hr></hr>

           <div className="card grey lighten-2 z-depth-5" style={{marginBottom: "50px"}}>
                <article className="card-body mx-auto z-depth-5"
                 style={{maxWidth: 600 + 'px', marginTop: "30px", marginBottom: "50px"}}>
                    <h4 className="card-title mt-3 text-center">LOG IN</h4>
                    <p className="text-center">Get started with your free account</p>
                    <p>
	                    	  <Link to="" className="btn btn-block secondary-color z-depth-1-half"
                                  style={{color: "white"}}>
                                  <i className="fab fa-twitter"></i>     Login via Twitter
                          </Link>
	                    	  <Link to="" className="btn btn-block secondary-color-dark z-depth-1-half"
                                  style={{color: "white"}}>
                                   <i className="fab fa-facebook-f"></i>     Login via facebook
                          </Link>
	                    </p>
                    <p className="divider-text">
                         <span className="bg-light">OR</span>
                    </p>

                    <form onSubmit={this.onSubmitForm} className="text-center"> 
                      <small className="text-danger" >{errorMessage}</small>
                      <div className="form-group input-group">
                        <div className="input-group-prepend z-depth-1">
                          <span className="input-group-text"  > <i className="fas fa-envelope fa-lg"></i> </span>
                        </div>
                        <input className="form-control z-depth-1"                                                
                          placeholder="Email address"                             
                          id="email"
                          name="email"
                          onChange={this.handlerChange} 
                          value={email} />
                      </div> 
                      {/* <small className="text-danger" >{errors["email"]}</small> */}
                   
                      <div className="form-group input-group">                         
                        <div className="input-group-prepend z-depth-1">
                            <span className="input-group-text"  > <i className="fas fa-lock fa-lg"></i> </span>
                        </div>                           
                        <input className="form-control z-depth-1"                                               
                          placeholder="Password"
                          type="password"
                          id="password"
                          name="password"
                          onChange={this.handlerChange}
                          value={password} />                              
                      </div> 
                      {/* <small className="text-danger"  >{errors["password"]}</small> */}

                      <div className="form-group">
                          <button type="submit" style={{color: "white"}}
                              className="btn  purple darken-3 btn-block z-depth-1-half">
                              Sign In  
                          </button>
                          <p className="text-center">Do not have account? <Link to="/register">Sign Up</Link> </p> 
                      </div>                           
                    </form>
                </article>
           </div>
       </div>
       {loading && <EclipseWidget />}
       </>
     );
  }
}
 
export default LoginPage;