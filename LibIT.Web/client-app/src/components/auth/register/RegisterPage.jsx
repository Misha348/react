import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import { Link } from 'react-router-dom';
//import { FieldValidation } from './validation';
import { serverUrl} from '../../Config';
import axios from 'axios';
import EclipseWidget from '../../common/loader-spinner';
//import LoginPage from '../login/LoginPage'
//import PropTypes from 'prop-types';


class RegisterPage extends Component {

  // static contextTypes = {
  //   router: PropTypes.object,
  // }
    state = {
      redirect: false, 
      loading: false,
      firstName: "",
      lastName: "",
      email: "",
      phoneNumber: "",
      password: "", 
      confirmPassword: "", 
      userImage: "https://www.justlandedcouriers.com/wp-content/uploads/2018/06/login-user-icon.png", 
      errorMessage: "",
      errors: {
          // firstName: "", 
          // lastName: "",
          // email: "",
          // phoneNumber: "",
          // password: "", 
          // confirmPassword: ""
      }     
    } 

    onSubmitForm = (e) => {
      e.preventDefault();

      // got value from state
      const {firstName, lastName, email, phoneNumber, password, confirmPassword, userImage,  redirect} = this.state;     
      let errors = {};

      const firstNameRegex = /[A-Za-z]{3,}/;
      const lastNameRegex = /[A-Za-z]{3,}/;
      const emailRegex = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/;
      const phoneRegex = /((\+38)?[\s]{1}?\d{3}?[\s]{1}?(\d{3}?[\s]{1}?\d{2}?[\s]{1}?\d{2}|\d{3}?[\s]{1}?\d{4}))/;
      const passwdRegex = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;

      if(userImage.trim() === "https://www.justlandedcouriers.com/wp-content/uploads/2018/06/login-user-icon.png")
          errors = { ...errors, userImage: "Chose photo"};

      if(firstName.trim() === "" || !firstNameRegex.test(firstName.trim()))        
         errors = { ...errors, firstName: "First name required at least 3 letters"};  

      if(lastName.trim() === "" || !lastNameRegex.test(lastName.trim()))      
          errors = {...errors, lastName: "Last name required at least 3 letters"}; 
    
      if (email.trim() === "" || !emailRegex.test(email.trim()))      
          errors = {...errors, email: "email is not valid"}; 
      
      if (phoneNumber.trim() === "" || !phoneRegex.test(phoneNumber.trim())) 
          errors = {...errors, phoneNumber: "Ph.number is not valid. Format: +38 0xx xxx xxxx"};     
         

      if (password.trim() === "" || !passwdRegex.test(password.trim()))      
          errors = {...errors, password: "Password is not valid; at least 1 Upper case letter; " + 
                                          "at least 1 Lowwer case letter; at least 1 digit; " + 
                                          " at least 1 theese sights: !@#)($%^&*"};

      if (confirmPassword.trim() === "" || confirmPassword !== password)   
          errors = {...errors, phoneNumber: "password confirmation invalid !"};   
      
      const isValid = Object.keys(errors).length === 0;
      if (isValid) {   
        const url = `${serverUrl}api/Account/register`;

        const model = {
          FirstName: firstName, 
          LastName: lastName,
          ImageBase64: userImage, 
          Email: email, 
          PhoneNumber: phoneNumber, 
          Password: password, 
          PasswordConfirmation: confirmPassword
        };

       
        axios.post(url, model)
                  .then(
                   (resp) => {
                     // this.context.router.history.push("/login");
                      this.setState({loading: false });
                      this.setState({ redirect: true })
                   },
                   (badResp) => {
                      this.setState({loading: false });
                   }
                  )
                 .catch(err => {
                     this.setState({loading: false });
                     console.log("Global server prooblem in controller message", err);
                 });
          

        this.setState({ errorMessage: "", errors, loading: true });
      }
      else {
          this.setState({ errors});
      }
     
    }

    handlerChange = (e) => {   
        
      this.setState({[e.target.name]: e.target.value});  
      console.log(e.target.name, e.target.value, e)   
    }

    photoHandlerChange = (e) => {
      var fieldName = e.target.name;     
     
      let files = e.target.files;     
      let reader = new FileReader();
      reader.readAsDataURL(files[0]);      
      reader.onload = (e) => {
        console.log(fieldName, e.target.result);
          this.setState({[fieldName]: e.target.result});  
       
      };
    }

    render() {       
       const {firstName, lastName, email, phoneNumber, password, confirmPassword, errorMessage, userImage, 
       errors, loading, redirect} = this.state;

       if (redirect) {
        return <Redirect to='/login'/>;
      }

        return (
          <>
            <div className="container  ">
              <br></br>
              <p className="text-center">More bootstrap 4 components on <Link to="http://bootstrap-ecommerce.com/"> Bootstrap-ecommerce.com</Link></p>
              <hr></hr>

              <div className="card z-depth-4 grey lighten-2" style={{ marginBottom: "50px" }}>             
                  <h4 className="card-title mt-3 text-center">Create Account</h4>
                  <p className="text-center">Get started with your free account</p>
                  <p className="text-center text-danger">{errorMessage}</p>

                  <div className="container">
                    <div className="row justify-content-center">                   
                      <div className="col-md-3">
                      <Link to="" className="btn btn-block btn-twitter">
                           <i className="fab fa-twitter"></i>    Login via Twitter
                        </Link>
                      </div>   
                      <div className="col-md-3">
                      <Link to="" className="btn btn-block btn-facebook">
                          <i className="fab fa-facebook-f"></i>    Login via facebook
                        </Link>
                      </div> 
                    </div>

                    <div className="row justify-content-center"> 
                      <div className="col-md-8">
                        <p className="divider-text">
                            <span className="bg-light">OR</span>
                        </p>
                        <hr style={{borderTop: "1px solid light-gray"}}></hr>
                      </div> 
                    </div>
                  </div>               

                 <div className="container">
                    <div className="col-md-10 mx-auto ">
                      <form onSubmit={this.onSubmitForm} className="text-center">

                        <div className="form-group row justify-content-center">
                          <div className="col-sm-5 text-center">

                            <label htmlFor="User photo" style={{cursor: "pointer"}}>                           
                             <div >
                              <img src={userImage}                       
                                 height="120" className="z-depth-4" style={{borderRadius: "50%"}}
                                 alt="..." />
                            </div>
                            </label>
                            <input       
                              style={{display: "none"}}
                              id="User photo"
                              type="file"
                              name="userImage"
                              accept='image/*'
                              className="form-control"
                              onChange={this.photoHandlerChange}/>                           
                          </div>
                        </div>
                        <small className="text-danger ">{errors["userImage"]}</small>


                        <div className="form-group row justify-content-center">

                          <div className="col-sm-5">
                            <label htmlFor="firstName">First name</label>
                              <div className="form-group input-group">                         
                                <div className="input-group-prepend z-depth-1">
                                  <span className="input-group-text"> <i className="fas fa-user-circle fa-lg"></i> </span>
                                </div>                           
                                <input className="form-control z-depth-1"                      
                                  placeholder="First name"
                                  type="text"
                                  id="firstName"
                                  name="firstName"
                                  onChange={this.handlerChange} 
                                  value={firstName} />                             
                              </div>
                              <small className="text-danger">{errors["firstName"]}</small>
                          </div>

                          <div className="col-sm-5">
                            <label className=""  htmlFor="email">Email address</label>
                              <div className="form-group input-group">
                                <div className="input-group-prepend z-depth-1">
                                  <span className="input-group-text"> <i className="fas fa-envelope fa-lg"></i> </span>
                                </div>
                                <input className="form-control z-depth-1"                     
                                  placeholder="Email address"                             
                                  id="email"
                                  name="email"
                                  onChange={this.handlerChange} 
                                  value={email} />
                              </div>                          
                              <small className="text-danger">{errors["email"]}</small>                                                 
                          </div>
                        </div>

                        <div className="form-group row justify-content-center">

                          <div className="col-sm-5">
                          <label className=""  htmlFor="lastName">Last name</label>
                            <div className="form-group input-group">
                              <div className="input-group-prepend z-depth-1">
                                <span className="input-group-text"> <i className="fas fa-user-circle fa-lg"></i> </span>
                              </div>
                              <input className="form-control z-depth-1"                     
                                placeholder="Last name"
                                type="text"
                                id="lastName"
                                name="lastName"
                                onChange={this.handlerChange} 
                                value={lastName} />
                            </div>   
                            <small className="text-danger">{errors["lastName"]}</small>                          
                          </div>

                          <div className="col-sm-5">
                          <label className=""  htmlFor="lastName">Phone number</label>
                           <div className="form-group input-group">
                              <div className="input-group-prepend z-depth-1">
                                  <span className="input-group-text"> <i className="fas fa-mobile-alt fa-lg"></i> </span>
                              </div>
                              <input className="form-control z-depth-1"                     
                                placeholder="Phone number"
                                type="text"
                                id="phoneNumber"                            
                                name="phoneNumber"
                                onChange={this.handlerChange}
                                value={phoneNumber}  />
                            </div>
                            <small className="text-danger">{errors["phoneNumber"]}</small> 
                          </div>

                        </div>
                        <div className="form-group row justify-content-center">

                          <div className="col-sm-5">
                          <label className=""  htmlFor="password">Password</label>
                            <div className="form-group input-group">
                              <div className="input-group-prepend z-depth-1">
                                <span className="input-group-text"> <i className="fas fa-lock fa-lg"></i> </span>
                              </div>
                              <input className="form-control z-depth-1"                     
                                placeholder="Password"
                                type="password"
                                id="password"
                                name="password"
                                onChange={this.handlerChange}
                                value={password} />
                            </div>
                            <small className="text-danger">{errors["password"]}</small>                                                    
                          </div>

                        </div>
                        <div className="form-group row justify-content-center">

                          <div className="col-sm-5">    
                          <label className=""  htmlFor="confirmPassword">Confirm password</label>                      
                            <div className="form-group input-group">                         
                              <div className="input-group-prepend z-depth-1">
                                <span className="input-group-text"> <i className="fas fa-lock fa-lg"></i> </span>
                              </div>
                              <input className="form-control z-depth-1"                     
                                placeholder="Confirm password"
                                type="password"
                                id="confirmPassword"
                                name="confirmPassword"
                                onChange={this.handlerChange}
                                value={confirmPassword} />
                            </div>   
                            <small className="text-danger">{errors["confirmPassword"]}</small>                                                  
                          </div>

                        </div>
                        <div className="form-group row justify-content-center">
                          <div className="col-sm-5">   
                            <div className="form-group">
                              <button type="submit" className="btn primary-color-dark btn-block text-white z-depth-1"> Create Account  </button>
                            </div>
                            <p className="text-center">Have an account? <Link to="/login">Log In</Link> </p>  
                          </div>
                        </div>
                      </form>                       
                    </div> 
                 </div>               
              </div>
            </div>
            {loading && <EclipseWidget />}
          </>
        );
    }
}

export default RegisterPage;