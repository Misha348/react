import React, {Component} from 'react';
import './index.scss';

class EclipseWidgetContainer extends Component {
    render() {
        return (
            <div className="my_eclipse" id="dlgProgress">
                <div className="progress text-center">
                    <div>
                        {/* <i className="fa fa-spinner fa-pulse fa-3x fa-fw"></i> */}
                        <i className="spinner-grow text-secondary d-flex justify-content-center"
                            style={{width: "10rem",  height: "10rem"}}></i>
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
        );
    }
}
const EclipseWidget = (EclipseWidgetContainer);
export default EclipseWidget;