import React, { Component } from "react";
import { Link } from "react-router-dom";

class Navbar extends Component {
  render() {
    return (
  <div>
    <nav className="navbar navbar-expand-lg navbar-dark special-color-dark">
      <Link className="navbar-brand" to="/">
       MENU
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#basicExampleNav"
        aria-controls="basicExampleNav"
        aria-expanded="false"
        aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="basicExampleNav">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/">Home</Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/">
              Page 1
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/">
            Page 2
            </Link>
          </li>         

          {/* <li className="nav-item dropdown">
            <Link className="nav-link dropdown-toggle"
              id="navbarDropdownMenuLink"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false">                    
              Dropdown
            </Link>
            <div className="dropdown-menu dropdown-primary"
              aria-labelledby="navbarDropdownMenuLink">
              <Link className="dropdown-item" to="/">
                Action
              </Link>
              <Link className="dropdown-item" to="/">
                Another action
              </Link>
              <Link className="dropdown-item" to="/">
                Something else here
              </Link>
            </div>
          </li> */}
        </ul>
          <form className="form-inline">              
            <div className="md-form my-0 nav-item">
                <Link className="nav-link waves-light" to="/login"
                    style={{color: "white"}}>
                    Login
                </Link> 
            </div>
            <div className="md-form my-0">
                <Link className="nav-link" to="/register"
                style={{color: "white"}}>
                    Sign Up
                </Link> 
            </div>
            <div className="md-form my-0">
              <input
                className="form-control mr-sm-2"
                type="text"
                placeholder="Search"
                aria-label="Search"/>    
            </div>
          </form>
      </div>
    </nav>  
</div>  

    );
  }
}

export default Navbar;
